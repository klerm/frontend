export default function authHeader() {
    let doctor = JSON.parse(localStorage.getItem('user'));

    if (doctor && doctor.accessToken) {
        return { Authorization: 'Bearer ' + doctor.accessToken };
    } else {
        return {};
    }
}
