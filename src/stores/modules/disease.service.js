import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8700/api/diseases/';

class Disease {

    getAllDiseases() {
        return axios.get(API_URL);
    }

    getDiseaseById(id) {
        return axios.get(API_URL + "disease/" + id);
    }

    async addNewDisease(disease) {
        return axios.post(API_URL, disease, { headers: authHeader() });
    }

    deleteDisease(id) {
        return axios.delete(API_URL + id, { headers: authHeader() });
    }

    updateDisease(updatedCategory) {

        let category = {
            name: updatedCategory.name
        }

        return axios.put(API_URL + updatedCategory.id, category, { headers: authHeader() });
    }
}

export default new Disease();