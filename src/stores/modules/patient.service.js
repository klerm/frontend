import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8700/api/patients/';

class Patient {

    getAllPatient() {
        return axios.get(API_URL);
    }

    getPatientById(id) {
        return axios.get(API_URL + "patient/" + id);
    }

    async addNewPatient(patient) {
        return axios.post(API_URL, patient, { headers: authHeader() });
    }

    deletePatient(id) {
        return axios.delete(API_URL + id, { headers: authHeader() });
    }

    updatePatient(updatedPatient) {

        let patient = {
            name: updatedPatient.name
        }

        return axios.put(API_URL + updatedPatient.id, patient, { headers: authHeader() });
    }
}

export default new Patient();