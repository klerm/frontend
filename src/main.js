import Vue from 'vue';
import App from './App.vue';
import { router } from './router';
import axios from 'axios';
import store from './store/index'


axios.defaults.baseURL = 'https://localhost:8700/';


Vue.config.productionTip = false

new Vue({
  router,
  store: store,
  render: h => h(App)
}).$mount('#app');
