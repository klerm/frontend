import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8700/api/patientsforms/';

class Patient {

    getAllPatientforms() {
        return axios.get(API_URL);
    }

    getPatientformById(id) {
        return axios.get(API_URL + "patientform/" + id);
    }

    async addNewPatientform(patientform) {
        return axios.post(API_URL, patientform, { headers: authHeader() });
    }

    deletePatientform(id) {
        return axios.delete(API_URL + id, { headers: authHeader() });
    }

    updatePatientform(updatedPatientform) {

        let patientform = {
            name: updatedPatient.name
        }

        return axios.put(API_URL + updatedPatientform.id, patientform, { headers: authHeader() });
    }
}

export default new Patient();