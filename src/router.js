import Vue from 'vue';
import VueRouter from 'vue-router'
import Login from './Views/Login.vue';
import Signup from './Views/Signup.vue';
import Home from './Views/Home.vue';

Vue.use(VueRouter);

export const router = new VueRouter({
    mode: 'history',
    routes: [{
            path: '/',
            component: Home
        },
        {
            path: '/login',
            component: Login
        },
        {
            path: '/signup',
            component: Signup
        },
        { path: '*', redirect: '/' }
    ]
});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login','/','/signup'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('doctor');
  
    if (authRequired && !loggedIn) {
      return next('/login');
    }
  
    next();
})
